# "Building Java Projects with Maven"

## Notas

- Mave wrapper: maven completo dentro del proyecto.
  - Permite varios desarrolladores usar misama versión Maven y evitar errores.
- Definición proyecto Maven o POM
  - Fichero XML.
  - Define nombre proyecto, version y dependencias.
  - Ubicación: raíz proyecto.
  - Configuraciones pom.xml básico:
    - `<modelVersion>`: versión del POM.
    - `<groupId>`: nombre compañia o grupo pertenece proyecto
      - Normalmente nombre dominio invertido (Ej.: org.apache)
    - `<artifactId>`: nombre binario o JAR resultante.
    - `<version>`: versión del proyecto actual.
    - `<packaging>`: como empaquetar proyecto.
      - por defecto -> jar
      - usar "war" para empaquetar como WAR.
- `mvn compile`
  - ejecuta la etapa o meta`compile`.
  - genera los ficheros `.class`.
- `mvn package`
  - ejecuta todas etapas hasta etapa `package`.
    - compila, ejecuta test y empaqueta en JAR.
    - usar `<artifactId>` y `<version>` para nombre JAR.
- Ejecutar JARs: `java -jar target/<artifactId>-<version>.jar>`
- `mvn install`
  - Ejecuta fases hasta `install`.
  - Compila, ejecuta tests, empaqueta y copia JAR en repositorio local.
-Añadir dependencias Maven
  - Dentro elemento `<proyect>` introducir `<dependencies><dependency>...</dependency><dependencies/>`
  - Cada elemento `<dependency>` tiene elementos obligatorios:
    - `<groupId>`: grupo o organización dueña.
    - `<artifactId>`: biblioteca requerida.
    - `<version>`: versión específica biblioteca requerida.
  - Elemento hijo `<scope>
    - Por defecto valor = "compile".
      - Dependencia esta disponible hora compilar.
    - Valor "provided"
      - dependencias necesarias compilar proyecto, pero serán proveidas tiempo ejecución por contenedor ejecutando código (Ej.: Java Servlet API).
    - Valor "test"
      - dependencias usadas para compilar y ejecutar pruebas, pero no necesarias para construir o ejecutar código en tiempo de ejecución.
- Principal biblioteca pruebas Java: `junit`
  - Nombre clases prueba mismo nombre clase a probar acabada en "Test".
    - Ej.: Greeter.java -> GreeterTest.java
  - Ubicadas en `src/test/java/nombrePaquete/`.
    - Configuración por defecto plugin Maven ejecución pruebas ("surefire").
- `mvn test`
  - Ejecuta fases Maven hasta tests.
    - Compila y ejecuta tests.
  - `mvn install` tambien ejecuta tests.


## Fuentes

- [Enlace](https://spring.io/guides/gs/maven/) a guía.